﻿namespace App
{
    class Follow
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public override string ToString() { return this.Name; }
    }
}