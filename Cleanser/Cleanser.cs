﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Core.Interfaces;

namespace App
{
    partial class Cleanser : Form
    {
        private IUser loggedUser;
        private List<Follow> followsList;
        private BindingSource listBindings;
        private int currentFollows;

        public Cleanser()
        {
            // Retrieves user using credentials found in app.config
            TwitterCredentials.SetCredentials(
                   ConfigurationManager.AppSettings["token_AccessToken"],
                   ConfigurationManager.AppSettings["token_AccessTokenSecret"],
                   ConfigurationManager.AppSettings["token_ConsumerKey"],
                   ConfigurationManager.AppSettings["token_ConsumerSecret"]);

            loggedUser = User.GetLoggedUser();

            // Don't start if API limit's been exceeded
            var rateLimit = RateLimit.GetCurrentCredentialsRateLimits().StatusesHomeTimelineLimit.Remaining;
            if (rateLimit > 0)
            {
                currentFollows = loggedUser.FriendsCount;
                DrawWindow();
            }
            else
            {
                MessageBox.Show("Twitter's API limit has been exceeded. Please try again in a few minutes.",
                                "API Limit Exceeded", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();

            bw.DoWork += new System.ComponentModel.DoWorkEventHandler(
            delegate(object o, System.ComponentModel.DoWorkEventArgs args)
            {
                System.ComponentModel.BackgroundWorker b = o as System.ComponentModel.BackgroundWorker;
                followsList = GetFollows();
            });

            bw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(
            delegate(object o, System.ComponentModel.RunWorkerCompletedEventArgs args)
            {
                listBindings = new BindingSource();
                listBindings.DataSource = followsList;
                userList.DataSource = listBindings;
            });

            bw.RunWorkerAsync();
        }

        private List<Follow> GetFollows()
        {
            // Limiting to avoid exceeding API limit
            var numUsers = 20;
            // Change to arrays to allow iteration
            object[] names = loggedUser.GetFriends(numUsers).ToArray();
            long[] ids = loggedUser.GetFriendIds(numUsers).ToArray();

            // Generate list and add each name with each ID
            List<Follow> follows = new List<Follow>();
            for (int i = 0; i < numUsers; i++)
            {
                var name = Convert.ToString(names[i]);
                var id = Convert.ToInt64(ids[i]);
                follows.Add(new Follow() { Name = name, Id = id });
            }

            return follows;
        }

        private int CalculateWeekTweets(string selectedUser)
        {
            var now = DateTime.Now;
            var yesterweek = now.AddDays(-7);

            var weekSearch = Search.GenerateSearchTweetParameter(selectedUser);
            weekSearch.Until = yesterweek;
            weekSearch.MaximumNumberOfResults = 350;

            return Search.SearchTweets(weekSearch).Count;
        }

        private int CalculateDayTweets(string selectedUser)
        {
            var now = DateTime.Now;
            var yesterday = now.AddDays(-1);

            var daySearch = Search.GenerateSearchTweetParameter(selectedUser);
            daySearch.Until = yesterday;
            daySearch.MaximumNumberOfResults = 120;

            return Search.SearchTweets(daySearch).Count;
        }

        private int CalculateHourTweets(string selectedUser)
        {
            var now = DateTime.Now;
            var yesterhour = now.AddHours(-1);

            var hourSearch = Search.GenerateSearchTweetParameter(selectedUser);
            hourSearch.Until = yesterhour;
            hourSearch.MaximumNumberOfResults = 30;

            return Search.SearchTweets(hourSearch).Count;
        }
    }
}
