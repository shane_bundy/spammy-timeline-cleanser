﻿namespace App
{
    partial class Cleanser
    {
        private System.Windows.Forms.Label aboutMe;
        private System.Windows.Forms.ListBox userList;
        private System.Windows.Forms.Button follow;
        private System.Windows.Forms.Button unfollow;
        private System.Windows.Forms.Label weeklyRate;
        private System.Windows.Forms.Label dailyRate;
        private System.Windows.Forms.Label hourlyRate;

        private void DrawWindow()
        {
            // Icon variable
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cleanser));

            this.aboutMe = new System.Windows.Forms.Label();
            this.aboutMe.Text = loggedUser + " is currently following " + currentFollows + " users.";
            this.aboutMe.Location = new System.Drawing.Point(10, 10);
            this.aboutMe.Size = new System.Drawing.Size(235, 15);

            // Container for list of follows
            this.userList = new System.Windows.Forms.ListBox();
            this.userList.Location = new System.Drawing.Point(10, 30);
            this.userList.Size = new System.Drawing.Size(235, 275);
            this.userList.DisplayMember = "Name";
            this.userList.ValueMember = "Id";
            this.userList.SelectedIndex = -1;
            this.userList.SelectedIndexChanged += (sender, ev) =>
            {
                var selectedId = userList.SelectedValue;
                var selectedUser = "@" + Tweetinvi.User.GetUserFromId((long)selectedId).ScreenName;
                var weekTweets = 0;
                var dayTweets = 0;
                var hourTweets = 0;
                    
                System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();

                bw.DoWork += new System.ComponentModel.DoWorkEventHandler(
                delegate(object o, System.ComponentModel.DoWorkEventArgs args)
                {
                    System.ComponentModel.BackgroundWorker b = o as System.ComponentModel.BackgroundWorker;

                    weekTweets = CalculateWeekTweets(selectedUser);
                    dayTweets = CalculateDayTweets(selectedUser);
                    hourTweets = CalculateHourTweets(selectedUser);
                });
                    
                bw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(
                delegate(object o, System.ComponentModel.RunWorkerCompletedEventArgs args)
                {
                    weeklyRate.Text = weekTweets + " tweets per week";
                    dailyRate.Text = dayTweets + " tweets per day";
                    hourlyRate.Text = hourTweets + " tweets per hour";
                });

                bw.RunWorkerAsync();
            };
            
            this.follow = new System.Windows.Forms.Button();
            this.follow.Enabled = false;  // Disabled for now
            this.follow.Text = "Follow";
            this.follow.Location = new System.Drawing.Point(50, 295);
            this.follow.Size = new System.Drawing.Size(75, 25);
            this.follow.Click += (sender, ev) =>
            {
                System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show(
                                                           "Are you sure you'd like to follow this user?", "Follow",
                                                           System.Windows.Forms.MessageBoxButtons.YesNo,
                                                           System.Windows.Forms.MessageBoxIcon.Question);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    followsList.Add(new Follow() { Name = userList.SelectedItem.ToString(), Id = (long)userList.SelectedValue });
                    listBindings.ResetBindings(true);
                    userList.Refresh();

                    var selectedUser = Tweetinvi.User.GetUserFromId((long)userList.SelectedValue);
                    Tweetinvi.User.GetLoggedUser().FollowUser(selectedUser);

                    currentFollows++;
                }
            };
          
            this.unfollow = new System.Windows.Forms.Button();
            this.unfollow.Text = "Unfollow";
            this.unfollow.Location = new System.Drawing.Point(125, 295);
            this.unfollow.Size = new System.Drawing.Size(75, 25);
            this.unfollow.Click += (sender, ev) =>
            {
                System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show(
                                                           "Are you sure you'd like to unfollow this user?", "Unfollow",
                                                           System.Windows.Forms.MessageBoxButtons.YesNo,
                                                           System.Windows.Forms.MessageBoxIcon.Question);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    followsList.Remove(new Follow() { Name = userList.SelectedItem.ToString(), Id = (long)userList.SelectedValue });
                    listBindings.ResetBindings(true);
                    userList.Refresh();  // Can't update listbox after following a user back

                    // Commented out until listbox can be updated
                    //var selectedUser = Tweetinvi.User.GetUserFromId((long)userList.SelectedValue);
                    //Tweetinvi.User.GetLoggedUser().UnFollowUser(selectedUser);

                    currentFollows--;
                }
            };

            // Displays selected user's weekly tweet rate
            this.weeklyRate = new System.Windows.Forms.Label();
            this.weeklyRate.Location = new System.Drawing.Point(10, 330);
            this.weeklyRate.Size = new System.Drawing.Size(235, 15);

            // Displays selected user's daily tweet rate
            this.dailyRate = new System.Windows.Forms.Label();
            this.dailyRate.Location = new System.Drawing.Point(10, 350);
            this.dailyRate.Size = new System.Drawing.Size(235, 15);

            // Displays selected user's hourly tweet rate
            this.hourlyRate = new System.Windows.Forms.Label();
            this.hourlyRate.Location = new System.Drawing.Point(10, 370);
            this.hourlyRate.Size = new System.Drawing.Size(235, 15);

            // Main window
            this.Text = "Spammy Timeline Cleanser";
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ClientSize = new System.Drawing.Size(255, 395);
            this.Controls.Add(this.aboutMe);
            this.Controls.Add(this.userList);
            this.Controls.Add(this.follow);
            this.Controls.Add(this.unfollow);
            this.Controls.Add(this.weeklyRate);
            this.Controls.Add(this.dailyRate);
            this.Controls.Add(this.hourlyRate);
        }
    }
}
